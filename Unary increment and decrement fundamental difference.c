 // Unary operators' fundamental difference.c
#include<stdio.h>

int main()
{
	int a=10, b=10, c=1;
	
	a = a++;
	b = ++b;
	
	printf("a after a++: %d\n", a);
	printf("b after b++: %d\n", b);
	
	for(c=1; c<a; ++c)
	{
		printf("c: %d\n", c);
	}
	
	return 0;
	
}







//
