// C program to illustrate 
// strcmp() function 
#include<stdio.h> 
#include<string.h> 
int main() 
{  
    // b has less ASCII value than g 
    char leftStr[20]; 
    char rightStr[20];
	
	printf("Enter your string: ");
	scanf("%s", leftStr);
	printf("Enter your string: ");
	scanf("%s", rightStr); 
      
    int res = strcmp(leftStr, rightStr); 
      
    if (res==0) 
        printf("Strings are equal"); 
    else 
        printf("Strings are unequal"); 
          
    printf("\nValue returned by strcmp() is:  %d" , res); 
      
      
    return 0; 
}
