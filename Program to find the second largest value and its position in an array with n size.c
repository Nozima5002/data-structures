// Program to find the second largest value and its position in an array with n size.c
#include<stdio.h>

int main()
{
	int arr[20], i, n, slarge, pos, large;
	
	printf("Enter a value for n: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		printf("Enter a value for arr[%d]=", i);
		scanf("%d", &arr[i]);
	}
	large=arr[0];
	for(i=0;i<n;i++)
	{
		if(arr[i]>large)
		{
			large=arr[i];
		}
	}
	printf("The largest element is %d\n", large);
	slarge=arr[1];
	pos=0;
	
	for(i=0;i<n;i++)
	{
		if(arr[i]!=large && slarge<arr[i])
		{
				slarge=arr[i];
				pos=i;
		}
	}
	
	printf("The second largest element is %d\n", slarge);
	printf("The position of the second largest element is %d\n", pos);
	
	return 0;
	
}
