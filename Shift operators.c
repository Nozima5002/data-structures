// Shift operators.c
#include<stdio.h>

int main()
{
	int n = 8;
	
	printf("8 << 2: %d\n", n<<2);
	printf("8 >> 2: %d\n", n>>2);
	
	return 0;
	
}
