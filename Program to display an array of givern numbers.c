// Program to display an array of givern numbers.c
#include<stdio.h>

int main()
{
	int arr[]={1,2,3,4,5,6,7,8,9};
	int *ptr, *ptr1;
	
	ptr = arr;
	ptr1 = &arr[8]; // how ptr1 has address of 9
	
	while(ptr<=ptr1)
	{
		printf("%d\n", *ptr);
		ptr++;
	}
	
	return 0;
}
