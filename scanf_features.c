// scanf_features.c
#include<stdio.h>

int main()
{
	int num, num1; // to declare two int data types
	
	printf("Enter the number: ");
	// scanf("%[*][width][modifier]type", &num);
	scanf("%2d", &num); // can take up to two numbers
	printf("The output of 2d is %d\n", num);
	
	scanf("%*d", &num1); // Does not stop to ask
	printf("The output of *d is %d\n", num1); // giving some randome value
	
	return 0;
}
