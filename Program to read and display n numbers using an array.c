// Program to read and display n numbers using an array.c
#include<stdio.h>

int main()
{
	int n, i, arr[20];
	
	printf("Enter the number for n value: ");
	scanf("%d", &n);

	for(i=0;i<n;i++)
	{	
		printf("Enter value for arr[%d]=", i);
		scanf("%d", &arr[i]);
	}
	
	printf("The values in the array are ");
	
	for(i=0;i<n;i++)
		printf("%d ", arr[i]);
		
	return 0;
}
