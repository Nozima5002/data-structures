// Program to merge two unsorted arrays into one.c
#include<stdio.h>

int main()
{
	int a1[10], a2[10], a3[20];
	int i, n1, n2, n3;
	
	printf("Enter a value for n1: ");
	scanf("%d", &n1);
	
	for(i=0; i<n1; i++)
		a1[i]=i;
	
	printf("Enter a value for n2: ");
	scanf("%d", &n2);
	
	for(i=0; i<n2; i++)
		a2[i]=i;
		
	n3=n1+n2;
	
	for(i=0;i<10;i++)
		a3[i]=a1[i];
		
	for(i=10;i<20;i++)
		a3[i]=a2[i-10];
	
	printf("The array we have now is ");
	for(i=0;i<n3;i++)
		printf("%d ", a3[i]);
		
	return 0;
}
