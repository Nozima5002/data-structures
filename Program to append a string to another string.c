// Program to append a string to another string.c
#include<stdio.h>

int main()
{
	char firstString[20], secondString[20];
	int i, j;
	
	printf("Enter the first string: ");
	gets(firstString);
	printf("Enter the second string: ");
	gets(secondString);
	
	while(firstString[i]!='\0')
		i++;
		
	while(secondString[j]!='\0')
	{
		firstString[i]=secondString[j];
		i++;
		j++;
	}
	firstString[i]='\0';
	
	printf("Your sting is ");
	puts(firstString);
	
	return 0;
	
}
