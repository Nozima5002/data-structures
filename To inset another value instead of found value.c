// To inset another value instead of found value.c
#include<stdio.h>

int main()
{
	int a[7], i, n=5, fnum, rnum, pos;
	
	for(i=0;i<n;i++)
	{
		printf("Enter an element in an array: ");
		scanf("%d", &a[i]);
	}
	
	printf("Enter the number you want to replace: ");
	scanf("%d", &fnum);
	
	for(i=0;i<n;i++)
	{
		if(a[i]==fnum)
		{
			pos=i;
			printf("Found\n");
			printf("Enter a desired number to replace: ");
			scanf("%d", &rnum);
			a[pos]=rnum;
			printf("Elements in the array after replacement are: ");
			// as it is to replace, there is no need to increase n
			for(i=0;i<n;i++)
			{
				printf("%d ", a[i]);
			}
			break;
		}
		else if(i==n-1 && a[i]!=fnum)
		{
			printf("Not Found!\n");	
		}		
	}
	
	return 0;
	
}
