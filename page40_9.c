// page40_9.c
// Grocery bill in c
#include<stdio.h>
#include<string.h>

int main()
{
	char arr_item[20][20], ch, currency[10];
	float price[20], quantity[20], amount[20], total;
	int i, n, l;
	
	printf("Enter the currency you will use please: ");
	scanf("%s", currency);

	printf("Enter how many items you want to buy first: ");
	scanf("%d", &n);
	
	for(i=0; i<n;i++)
	{
		printf("Enter a name of item: ");
		scanf("%s", arr_item[i]);
		printf("Enter a quantity of item: ");
		scanf("%f", &quantity[i]);
		printf("Enter a price of item: ");
		scanf("%f", &price[i]);
		amount[i] = quantity[i] * price[i];
		total+=amount[i];
	}
	
	for(i=0; i<n;i++)
	{
		printf("%s\t%.1f\t%.1f\t%.1f %s\n", arr_item[i], quantity[i], price[i], amount[i], currency);
	}
	
	printf("Total: %.2f %s\n", total, currency);
	return 0;	
}
