// pointersinsidewitharray.c
#include<stdio.h>

int main()
{
	int a[]={1,2,3,4,5};
	int *ptr,i;
	ptr=&a[2]; // location is 2nd which goes to 3
	printf("ptr = &a[2]: %d\n", *ptr); // using * gives decimal number at the location of 2nd which is 3
	*ptr=-1; // here *ptr means a decimal number and it is turning into -1 so the 2nd location now -1
	printf("*ptr=-1: %d\n", *ptr); // the output is -1
	*(ptr+1)=0; // after the second location third comes and so at the third location 4 becomes into 0
	printf("*(ptr+1)=0: %d\n", *(ptr+1)); // the out put is 0
	*(ptr-1)=1; // the ptr is 2nd location after minus it is the first location where the num 2 is at 
	printf("*(ptr-1)=1: %d\n\n", *(ptr-1)); // gives 1
	for(i=0;i<5;i++)
		printf("%d ", *(a+i));
	
	return 0;
}
