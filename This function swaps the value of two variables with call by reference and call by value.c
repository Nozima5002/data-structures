// This function swaps the value of two variables with call by reference and call by value.c
#include<stdio.h>

int swap_by_val(int, int);	// FUNCTION DECLARATION
int swap_by_ref(int *, int *);	// FUNCTION DECLORATION

int main()
{
	int a=1, b=2, c=3, d=4;
	
	printf("Swap in the case of value\n");
	printf("In main(), a = %d, b = %d\n", a, b);
	swap_by_val(a, b); // FUNCTION CALL
	printf("In main(), a = %d, b = %d\n", a, b);
	
	printf("Swap in the case of reference\n");
	printf("In main(), c = %d, d = %d\n", c, d);
	swap_by_ref(&c, &d); // FUNCTION CALL
	printf("In main(), c = %d, d = %d\n", c, d);
	
	return 0;
}

int swap_by_val(int a, int b) // CHANGES TAKE PLACE ONLY HERE
{
	a = a+b;
	b = a-b;
	a = a-b;
	printf("In call_by_val() a = %d, b = %d\n", a, b);
	// THIS SWAPPING METHOD IS MORE EFFICIENT AS IT SAVES MEMORY SPACE MORE
}

int swap_by_ref(int *c, int *d) // CHANGES TAKE PLACE IN ADDRESS LEVEL SO IT CHANGES PERMANANTLY
{
	*c = *c+*d;
	*d = *c-*d;
	*c = *c-*d;
	printf("In call_by_ref() c = %d, d = %d\n", *c, *d); 
	// THIS SWAPPING METHOD IS MORE EFFICIENT AS IT SAVES MEMORY SPACE MORE
}



//
