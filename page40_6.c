// page40_6.c
// Write a program to read a floating point number.
// Display the rightmost digit of the integral part of the number
#include<stdio.h>

int main()
{
	float num;
	printf("Enter a float number please: ");
	scanf("%f", &num);
	printf("The rightmost digit is %.1f\n", num);
	
	return 0;
}
