// Program to find the length of a string.c
// Use string.h for some string function like to find the length of a string
#include<stdio.h>
#include<string.h>

int main()
{
	char str[100], sarr[20];
	int length, len, i=0;
	printf("The first way with string.h lib:");
	printf("Enter a string here: ");
	gets(str);
	len = strlen(str);
	printf("The len is %d\n", len);
	
	printf("The second way with a while loop:");
	printf("Enter a string here: ");
	scanf("%s", sarr);
	while(sarr[i]!='\0')
	{
		i++;
		length=i;
	}
	printf("The length is %d\n", length);
	
	return 0;
}
