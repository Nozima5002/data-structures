// page40_2.c
// Write a program to print the count of even numbers
// between 1�200. Also print their sum.

#include<stdio.h>

int main()
{
	int i = 1, n = 200, count = 0, sum = 0;
	for(i=1;i<n;i++)
	{
		if(i%2==0)
		{
			count++;
			sum+=i;
		}
	}
	
	printf("The number of even numbers between 1 and 200 is %d\n", count);
	printf("The sum of even numbers between 1 and 200 is %d\n", sum);
	
	return 0;
}
