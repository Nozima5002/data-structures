// Program to find whether a number is even or odd using functions.c
#include<stdio.h>

int evenodd(int);	// FUNCTION DECLORATION

int main()
{
	int num, flag;
	printf("Enter teh number: ");
	scanf("%d", &num);
	flag = evenodd(num);	// FUNCTION CALL
	if(flag == 1)
		printf("%d is even\n", num);
	else
		printf("%d is odd\n", num);
	
	return 0;
}

int evenodd(int a)	// FUNCTION HEADER
{
	// FUNCTION BODY
	if(a%2 == 0)
		return 1;
	else
		return 0;
}
