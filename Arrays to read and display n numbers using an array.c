// Arrays to read and display n numbers using an array.c
#include<stdio.h>

int main()
{
	int i, n, arr[15]; // TO DECLARE VARIABLES
	
	printf("Enter the number of elements in the arry: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		printf("arr[%d] = ", i); // TO SHOW WHICH LOCATION
		scanf("%d", &arr[i]); // TO GAIN A VALUE
	}
	
	printf("The arrs: ");
	
	for(i=0;i<n;i++)
	{
		printf("%d ", arr[i]); // TO SHOW A LIST OF THE ARRAY
	}
		
	return 0;
} 
