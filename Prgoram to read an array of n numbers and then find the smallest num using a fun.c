// Prgoram to read an array of n numbers and then find the smallest num using a fun.c
#include<stdio.h>

int smallNumber(int arr[], int n, int i);
void readNumber(int arr[], int n, int i);

int main()
{
	int arr[10], i, n, num;
	
	printf("Enter a value for n: ");
	scanf("%d", &n);	// n must be inserted from here otherwise can't work
	
	readNumber(arr, n, i);
	num = smallNumber(arr, n, i);
	
	printf("The smallest number in this array is %d\n", num);
	
	return 0;
	
}

void readNumber(int arr[10], int n, int i)
{	
	for(i=0;i<n;i++)
		scanf("%d", &arr[i]);
}

int smallNumber(int arr[10], int n, int i)
{
	int small;
	small = arr[0];
	
	for(i=0;i<n;i++)
	{
		if(small>arr[i])
		{
			small = arr[i];
		}
	}
	
	return small;
}
