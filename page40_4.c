// page40_4.c
// Write a program to read the address of a user. Display the result by breaking it in multiple lines.
#include<stdio.h>
#include<string.h>

int main()
{
	char address[50];
	int i, n;
	printf("Enter your address please: ");
	gets(address); // to get a text like string
	// scanf("%s", address); // to get a string until a space
	// get(address); to get a character 
	n = strlen(address);
	for(i=0;i<n;i++)
	{
		if(address[i]==' ')
			address[i]='\n'; // to turn a space into a new line
		printf("%c", address[i]);
	}
	
	return 0;
}











//
