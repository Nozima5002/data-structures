// page41_15.c
//  program to find whether a number is divisible by two or not using functions

#include<stdio.h>

int check(int);

int main()
{
	int num;
	printf("Enter a number: ");
	scanf("%d", &num);
	check(num);

	return 0;
}

int check(int num)
{
	if(num%2 == 0)
		printf("Divisible\n");
	else
		printf("Not divisible\n");
}
