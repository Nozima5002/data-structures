// page40_10.c
// Write a C program using printf statement to print BYE in the following format.c

#include<stdio.h>

int main()
{
	printf("BBB  Y   Y   EEEE\n");
	printf("B  B  Y Y    E\n");
	printf("BBB    Y     EEEE\n");
	printf("B  B   Y     E\n");
	printf("BBB    Y     EEEE\n");
	
	return 0;
}
