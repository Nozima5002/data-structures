// passing data values in array into another function.c
#include<stdio.h>

void func(int num);

int main()
{
	int a[5]={1,2,3,4,5};
	func(a[2]);
}

void func(int num) // data type must be the same
{
	printf("The num is %d\n", num);
}
